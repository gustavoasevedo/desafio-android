package com.dss.desafio_android.model;

import com.dss.sdatabase.annotations.BaseDBFieldName;
import com.dss.sdatabase.annotations.BaseDBMethodGetName;
import com.dss.sdatabase.annotations.BaseDBMethodSetName;
import com.dss.sdatabase.annotations.BaseDBPrimaryKey;
import com.dss.sdatabase.annotations.BaseDBType;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Gustavo on 09/06/2016.
 */

public class Pull {

    @SerializedName("id")
    @BaseDBFieldName("id")
    @BaseDBType("INTEGER")
    @BaseDBPrimaryKey
    private int id;

    @SerializedName("html_url")
    @BaseDBFieldName("url")
    @BaseDBType("TEXT")
    private String url;

    @SerializedName("body")
    @BaseDBFieldName("body")
    @BaseDBType("TEXT")
    private String body;

    @SerializedName("title")
    @BaseDBFieldName("title")
    @BaseDBType("TEXT")
    private String title;

    @SerializedName("created_at")
    @BaseDBFieldName("date")
    @BaseDBType("TEXT")
    private String date;

    @BaseDBFieldName("idUser")
    @BaseDBType("INTEGER")
    private int idUser;

    @BaseDBFieldName("idRepo")
    @BaseDBType("INTEGER")
    private int idRepo;

    @SerializedName("user")
    private User user;

    public Pull (){

    }

    public Pull(int id, String url, User user, String body, String title, String date) {
        this.id = id;
        this.url = url;
        this.setUser(user);
        this.body = body;
        this.title = title;
        this.date = date;
    }

    @BaseDBMethodGetName("id")
    public int getId() {
        return id;
    }

    @BaseDBMethodSetName("id")
    public void setId(int id) {
        this.id = id;
    }

    @BaseDBMethodGetName("url")
    public String getUrl() {
        return url;
    }

    @BaseDBMethodSetName("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @BaseDBMethodGetName("body")
    public String getBody() {
        return body;
    }

    @BaseDBMethodSetName("body")
    public void setBody(String body) {
        this.body = body;
    }

    @BaseDBMethodGetName("title")
    public String getTitle() {
        return title;
    }

    @BaseDBMethodSetName("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @BaseDBMethodGetName("date")
    public String getDate() {
        return date;
    }

    @BaseDBMethodSetName("date")
    public void setDate(String date) {
        this.date = date;
    }

    @BaseDBMethodGetName("idUser")
    public int getIdUser() {
        return idUser;
    }

    @BaseDBMethodSetName("idUser")
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @BaseDBMethodGetName("idRepo")
    public int getIdRepo() {
        return idRepo;
    }

    @BaseDBMethodSetName("idRepo")
    public void setIdRepo(int idRepo) {
        this.idRepo = idRepo;
    }
}
