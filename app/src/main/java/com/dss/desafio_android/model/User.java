package com.dss.desafio_android.model;

import com.dss.sdatabase.annotations.BaseDBFieldName;
import com.dss.sdatabase.annotations.BaseDBMethodGetName;
import com.dss.sdatabase.annotations.BaseDBMethodSetName;
import com.dss.sdatabase.annotations.BaseDBPrimaryKey;
import com.dss.sdatabase.annotations.BaseDBType;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Gustavo on 09/06/2016.
 */

public class User {

    @SerializedName("id")
    @BaseDBFieldName("id")
    @BaseDBType("INTEGER")
    @BaseDBPrimaryKey
    private int id;

    @SerializedName("login")
    @BaseDBFieldName("login")
    @BaseDBType("TEXT")
    private String  login;

    @SerializedName("avatar_url")
    @BaseDBFieldName("avatar")
    @BaseDBType("TEXT")
    private String avatar;

    public User(){

    }

    public User(String avatar, int id, String login) {
        this.avatar = avatar;
        this.id = id;
        this.login = login;
    }


    @BaseDBMethodGetName("id")
    public int getId() {
        return id;
    }

    @BaseDBMethodSetName("id")
    public void setId(int id) {
        this.id = id;
    }

    @BaseDBMethodGetName("login")
    public String getLogin() {
        return login;
    }

    @BaseDBMethodSetName("login")
    public void setLogin(String login) {
        this.login = login;
    }

    @BaseDBMethodGetName("avatar")
    public String getAvatar() {
        return avatar;
    }

    @BaseDBMethodSetName("avatar")
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
