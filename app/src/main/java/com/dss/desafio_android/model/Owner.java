package com.dss.desafio_android.model;

import com.dss.sdatabase.annotations.BaseDBFieldName;
import com.dss.sdatabase.annotations.BaseDBMethodGetName;
import com.dss.sdatabase.annotations.BaseDBMethodSetName;
import com.dss.sdatabase.annotations.BaseDBPrimaryKey;
import com.dss.sdatabase.annotations.BaseDBType;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Gustavo on 09/06/2016.
 */

public class Owner {
    @SerializedName("id")
    @BaseDBFieldName("id")
    @BaseDBType("INTEGER")
    @BaseDBPrimaryKey
    private int id;

    @SerializedName("login")
    @BaseDBFieldName("login")
    @BaseDBType("TEXT")
    private String login; // usado para pegar os pulls

    @SerializedName("url")
    @BaseDBFieldName("url")
    @BaseDBType("TEXT")
    private String url;

    @SerializedName("avatar_url")
    @BaseDBFieldName("avatar")
    @BaseDBType("TEXT")
    private String avatar;

    public Owner(){

    }

    public Owner(int id, String login, String url, String avatar) {
        this.id = id;
        this.login = login;
        this.url = url;
        this.avatar = avatar;
    }

    @BaseDBMethodGetName("id")
    public int getId() {
        return id;
    }

    @BaseDBMethodSetName("id")
    public void setId(int id) {
        this.id = id;
    }

    @BaseDBMethodGetName("login")
    public String getLogin() {
        return login;
    }

    @BaseDBMethodSetName("login")
    public void setLogin(String login) {
        this.login = login;
    }

    @BaseDBMethodGetName("url")
    public String getUrl() {
        return url;
    }

    @BaseDBMethodSetName("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @BaseDBMethodGetName("avatar")
    public String getAvatar() {
        return avatar;
    }

    @BaseDBMethodSetName("avatar")
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
