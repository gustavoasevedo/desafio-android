package com.dss.desafio_android.model;

import com.dss.sdatabase.annotations.BaseDBFieldName;
import com.dss.sdatabase.annotations.BaseDBMethodGetName;
import com.dss.sdatabase.annotations.BaseDBMethodSetName;
import com.dss.sdatabase.annotations.BaseDBPrimaryKey;
import com.dss.sdatabase.annotations.BaseDBType;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Gustavo on 09/06/2016.
 */

public class Repository implements Serializable{

    @SerializedName("id")
    @BaseDBFieldName("id")
    @BaseDBType("INTEGER")
    @BaseDBPrimaryKey
    private int id;

    @SerializedName("name")
    @BaseDBFieldName("name")
    @BaseDBType("TEXT")
    private String name; // usado para pegar os pulls

    @SerializedName("full_name")
    @BaseDBFieldName("fullName")
    @BaseDBType("TEXT")
    private String fullName;

    @SerializedName("url")
    @BaseDBFieldName("url")
    @BaseDBType("TEXT")
    private String url;

    @SerializedName("language")
    @BaseDBFieldName("language")
    @BaseDBType("TEXT")
    private String language;

    @SerializedName("description")
    @BaseDBFieldName("description")
    @BaseDBType("TEXT")
    private String description;

    @SerializedName("forks_count")
    @BaseDBFieldName("forks")
    @BaseDBType("INTEGER")
    private int forks;

    @SerializedName("stargazers_count")
    @BaseDBFieldName("stargazers")
    @BaseDBType("INTEGER")
    private int stargazers;


    @BaseDBFieldName("idOwner")
    @BaseDBType("INTEGER")
    private int idOwner;

    @BaseDBFieldName("page")
    @BaseDBType("INTEGER")
    private int page;


    @SerializedName("owner")
    private Owner owner;



    public Repository(){

    }

    public Repository(int id, String name, String fullName, String url, String language,
                      String description, int forks, int stargazers,int idOwner, Owner owner) {
        this.id = id;
        this.setName(name);
        this.fullName = fullName;
        this.url = url;
        this.language = language;
        this.description = description;
        this.forks = forks;
        this.stargazers = stargazers;
        this.idOwner = idOwner;
        this.owner = owner;
    }

    @BaseDBMethodGetName("id")
    public int getId() {
        return id;
    }

    @BaseDBMethodSetName("id")
    public void setId(int id) {
        this.id = id;
    }

    @BaseDBMethodGetName("name")
    public String getName() {
        return name;
    }

    @BaseDBMethodSetName("name")
    public void setName(String name) {
        this.name = name;
    }

    @BaseDBMethodGetName("fullName")
    public String getFullName() {
        return fullName;
    }

    @BaseDBMethodSetName("fullName")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @BaseDBMethodGetName("url")
    public String getUrl() {
        return url;
    }

    @BaseDBMethodSetName("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @BaseDBMethodGetName("language")
    public String getLanguage() {
        return language;
    }

    @BaseDBMethodSetName("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @BaseDBMethodGetName("description")
    public String getDescription() {
        return description;
    }

    @BaseDBMethodSetName("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @BaseDBMethodGetName("forks")
    public int getForks() {
        return forks;
    }

    @BaseDBMethodSetName("forks")
    public void setForks(int forks) {
        this.forks = forks;
    }

    @BaseDBMethodGetName("stargazers")
    public int getStargazers() {
        return stargazers;
    }

    @BaseDBMethodSetName("stargazers")
    public void setStargazers(int stargazers) {
        this.stargazers = stargazers;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @BaseDBMethodGetName("idOwner")
    public int getIdOwner() {
        return idOwner;
    }

    @BaseDBMethodSetName("idOwner")
    public void setIdOwner(int idOwner) {
        this.idOwner = idOwner;
    }

    @BaseDBMethodGetName("page")
    public int getPage() {
        return page;
    }

    @BaseDBMethodSetName("page")
    public void setPage(int page) {
        this.page = page;
    }
}
