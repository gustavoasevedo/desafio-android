package com.dss.desafio_android.dto;

import com.dss.desafio_android.model.Pull;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PullsList {

    public static String setHeaderJson(String header, String json){
        return "{" + '"' + header + '"' + ":" + json + "}";
    }

    @SerializedName("items")
    public ArrayList<Pull> list = new ArrayList<>();

}
