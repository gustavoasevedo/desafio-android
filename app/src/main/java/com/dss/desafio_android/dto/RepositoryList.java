package com.dss.desafio_android.dto;

import com.dss.desafio_android.model.Repository;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RepositoryList {

    public static String setHeaderJson(String header, String json){
        return "{" + '"' + header + '"' + ":" + json + "}";
    }

    @SerializedName("items")
    public ArrayList<Repository> list = new ArrayList<>();

}
