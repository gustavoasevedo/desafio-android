package com.dss.desafio_android.tasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.dss.desafio_android.R;
import com.dss.desafio_android.constant.ConstantUrl;
import com.dss.desafio_android.dao.OwnerDao;
import com.dss.desafio_android.dao.RepositoryDao;
import com.dss.desafio_android.delegate.UpdateDelegate;
import com.dss.desafio_android.dto.RepositoryList;
import com.dss.desafio_android.model.Owner;
import com.dss.desafio_android.model.Repository;
import com.dss.desafio_android.util.HttpUtil;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;


public class ReposSyncTask extends AsyncTask<Void, Integer, Boolean> {

    private Exception erro;
    private int totalPorcentagem;
    private UpdateDelegate delegate;
    private int mPage;
    private String linguagem;
    ProgressDialog progress;

    public ReposSyncTask(int page, String linguagem, UpdateDelegate updateDelegate) {
        this.mPage = page;
        this.delegate = updateDelegate;
        this.linguagem = linguagem;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            progress = new ProgressDialog(delegate.getContext(), android.R.style.Theme_Material_Light_Dialog);
//        }else{
//            progress = new ProgressDialog(delegate.getContext());
//        }
//        progress.setCancelable(false);
//        progress.setTitle(delegate.getContext().getString(R.string.aguarde));
//        progress.setMessage(delegate.getContext().getString(R.string.carregando_listas));
//        progress.show();

    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            String url = ConstantUrl.getUrlLanguages() + linguagem + ConstantUrl.getUrlLanguagesSort() + mPage;

            String result = HttpUtil.getExecute(url);

            if (result != null && !result.equals("[]") && !result.equals("null")) {
                Gson serializer = new Gson();

                JSONObject jResp = new JSONObject(result);

                int totalCount = jResp.getInt("total_count");
                int totalStored = RepositoryDao.getInstance(delegate.getContext()).selectListbyLanguage(linguagem).size();
                if(totalCount == totalStored && mPage != 1){
                    this.erro = new Exception("Todos os dados foram atualizados.");
                    return false;
                }else{
                String itemsList = jResp.getJSONArray("items").toString();

                RepositoryList repositories = serializer.fromJson(RepositoryList.setHeaderJson("items", itemsList), RepositoryList.class);

                ArrayList<Owner> owners = new ArrayList<>();
                ArrayList<Repository> repositoryArrayList = new ArrayList<>();

                for(Repository repository : repositories.list){
                    repository.setIdOwner(repository.getOwner().getId());
                    repository.setPage(mPage);
                    owners.add(repository.getOwner());
                    repositoryArrayList.add(repository);
                }

                RepositoryDao.getInstance(delegate.getContext()).insertListObject(repositoryArrayList);
                OwnerDao.getInstance(delegate.getContext()).insertListObject(owners);

            }
            } else {
                if (delegate.getContext() != null) {
                    this.erro = new Exception("Erro ao baixar dados de Repositorio.");
                    return false;
                }
            }

            return true;
        }catch (Exception e){
            this.erro = new Exception("Erro ao baixar dados de Repositorio.");
            return false;
        }

    }

    @Override
    protected void onPostExecute(Boolean sucesso) {

        if (sucesso) {
            delegate.sucessoUpdate(sucesso);
        } else {
            delegate.ErroUpdate(erro);
        }
    }

}
