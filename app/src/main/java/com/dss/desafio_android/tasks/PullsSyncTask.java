package com.dss.desafio_android.tasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.dss.desafio_android.R;
import com.dss.desafio_android.constant.ConstantUrl;
import com.dss.desafio_android.dao.PullDao;
import com.dss.desafio_android.dao.UserDao;
import com.dss.desafio_android.delegate.UpdateDelegate;
import com.dss.desafio_android.dto.PullsList;
import com.dss.desafio_android.model.Pull;
import com.dss.desafio_android.model.User;
import com.dss.desafio_android.util.HttpUtil;
import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;


public class PullsSyncTask extends AsyncTask<Void, Integer, Boolean> {

    private Exception erro;
    private UpdateDelegate delegate;
    private String login;
    private String name;
    private int idRepo;
    ProgressDialog progress;

    public PullsSyncTask(String login, String name,int idRepo, UpdateDelegate delegate) {
        this.login = login;
        this.name = name;
        this.idRepo = idRepo;
        this.delegate = delegate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            progress = new ProgressDialog(delegate.getContext(), android.R.style.Theme_Material_Light_Dialog);
        }else{
            progress = new ProgressDialog(delegate.getContext());
        }
        progress.setCancelable(false);
        progress.setTitle(delegate.getContext().getString(R.string.aguarde));
        progress.setMessage(delegate.getContext().getString(R.string.carregando_listas));
        progress.show();

    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            String url = ConstantUrl.getUrlPulls() + login +"/" + name +  ConstantUrl.getUrlPullsGet();

            String result = HttpUtil.getExecute(url);

            if (result != null && !result.equals("[]") && !result.equals("null")) {
                Gson serializer = new Gson();

                PullsList pulls = serializer.fromJson(PullsList.setHeaderJson("items", result), PullsList.class);

                    ArrayList<User> users = new ArrayList<>();
                    ArrayList<Pull> pullArrayList = new ArrayList<>();

                    for(Pull pull : pulls.list){
                        pull.setIdUser(pull.getUser().getId());
                        pull.setIdRepo(idRepo);
                        users.add(pull.getUser());
                        pullArrayList.add(pull);
                    }

                    PullDao.getInstance(delegate.getContext()).insertListObject(pullArrayList);
                    UserDao.getInstance(delegate.getContext()).insertListObject(users);

            } else {
                if (delegate.getContext() != null) {
                    this.erro = new Exception("Erro ao baixar dados do Repo.");
                    return false;
                }
            }

            return true;
        }catch (Exception e){
            this.erro = new Exception("Erro ao baixar dados do Repo.");
            return false;
        }

    }

    @Override
    protected void onPostExecute(Boolean sucesso) {

        progress.dismiss();

        if (sucesso) {
            delegate.sucessoUpdate(sucesso);
        } else {
            delegate.ErroUpdate(erro);
        }
    }

}
