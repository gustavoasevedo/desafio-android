package com.dss.desafio_android.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.dss.desafio_android.R;
import com.dss.desafio_android.constant.ConstantFragment;
import com.dss.desafio_android.dao.OwnerDao;
import com.dss.desafio_android.dao.PullDao;
import com.dss.desafio_android.dao.RepositoryDao;
import com.dss.desafio_android.dao.UserDao;
import com.dss.desafio_android.fragment.FragmentStarter;
import com.dss.desafio_android.fragment.PullListFragment;
import com.dss.desafio_android.fragment.RepositoryListFragment;
import com.dss.desafio_android.helper.MainActivityHelper;

public class MainActivity extends ActionBarActivity {

    MainActivityHelper helper;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        helper = new MainActivityHelper();
        helper.MainActivity(this);
        helper.startNavigation(this);
        helper.setText(this);
    }
}
