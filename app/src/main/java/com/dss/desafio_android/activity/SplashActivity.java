package com.dss.desafio_android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;

import com.dss.desafio_android.R;
import com.dss.desafio_android.constant.ConstantUrl;
import com.dss.desafio_android.delegate.UpdateDelegate;
import com.dss.desafio_android.tasks.ReposSyncTask;

public class SplashActivity extends ActionBarActivity  implements UpdateDelegate{

    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 2500;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash);

        ReposSyncTask reposSyncTask = new ReposSyncTask(1, ConstantUrl.getUrlLanguagesJava(),this);
        reposSyncTask.execute();

        reposSyncTask = new ReposSyncTask(1, ConstantUrl.getURL_LANGUAGES_Python(),this);
        reposSyncTask.execute();

        reposSyncTask = new ReposSyncTask(1, ConstantUrl.getURL_LANGUAGES_Ruby(),this);
        reposSyncTask.execute();

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public void sucessoUpdate(boolean sucesso) {

    }

    @Override
    public void ErroUpdate(Exception e) {

    }

    @Override
    public Context getContext() {
        return this;
    }
}