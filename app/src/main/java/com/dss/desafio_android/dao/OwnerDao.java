package com.dss.desafio_android.dao;

import android.content.Context;

import com.dss.desafio_android.constant.ConstantDB;
import com.dss.desafio_android.model.Owner;
import com.dss.sdatabase.dao.BaseTable;
import com.dss.sdatabase.exceptions.InvalidTypeException;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;


public class OwnerDao extends BaseTable {

    private static OwnerDao instance;

    public OwnerDao(Context context) {
        super(context, Owner.class, ConstantDB.getDbName(),ConstantDB.getVersion());
        createTable();
    }

    public static OwnerDao getInstance(Context context) {
        if (instance == null) {
            synchronized (OwnerDao.class) {
                if (instance == null) {
                    instance = new OwnerDao(context);
                }
            }
        }
        return instance;
    }

    public void insertObject(Owner owner){

        ArrayList<Object> objectArrayList = new ArrayList<>();

        objectArrayList.add(owner);

        insert(objectArrayList);
    }

    public void insertListObject(ArrayList<Owner> owners){

        ArrayList<Object> objectArrayList = new ArrayList<>();

        for(Object object: owners){
            objectArrayList.add(object);
        }

        insert(objectArrayList);
    }


    public Owner selectId(int Id){

        Object object = new Object();
        Owner owner = new Owner();


        ArrayList<String> fields = new ArrayList<>();

        fields.add("id");


        String[] values = {String.valueOf(Id)};

        try {

            object = selectWhere(Owner.class,fields,values);

            owner = (Owner) object;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return owner;

    }


    public ArrayList<Owner> selectList(){
        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<Owner> lista = new ArrayList<>();

        try {
            objectList = selectList(Owner.class);

            lista = new ArrayList<Owner>();

            for(Object object : objectList){
                Owner owner = (Owner) object;
                lista.add(owner);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }


    public ArrayList<Owner> selectListbyName(String nome){

        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<Owner> lista = new ArrayList<>();

        ArrayList<String> fields = new ArrayList<>();

        fields.add("name");

        String[] values = {nome};

        try {
            objectList = selectListWhere(Owner.class,fields,values);

            lista = new ArrayList<Owner>();

            for(Object object : objectList){
                Owner owner = (Owner) object;
                lista.add(owner);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }



}
