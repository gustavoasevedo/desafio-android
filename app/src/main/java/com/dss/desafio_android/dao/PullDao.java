package com.dss.desafio_android.dao;

import android.content.Context;

import com.dss.desafio_android.constant.ConstantDB;
import com.dss.desafio_android.model.Pull;
import com.dss.sdatabase.dao.BaseTable;
import com.dss.sdatabase.exceptions.InvalidTypeException;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class PullDao extends BaseTable {

    private static PullDao instance;

    public PullDao(Context context) {
        super(context, Pull.class, ConstantDB.getDbName(),ConstantDB.getVersion());
        createTable();
    }

    public static PullDao getInstance(Context context) {
        if (instance == null) {
            synchronized (PullDao.class) {
                if (instance == null) {
                    instance = new PullDao(context);
                }
            }
        }
        return instance;
    }

    public void insertObject(Pull pull){

        ArrayList<Object> objectArrayList = new ArrayList<>();

        objectArrayList.add(pull);

        insert(objectArrayList);
    }

    public void insertListObject(ArrayList<Pull> pulls){

        ArrayList<Object> objectArrayList = new ArrayList<>();

        for(Object object: pulls){
            objectArrayList.add(object);
        }

        insert(objectArrayList);
    }


    public Pull selectId(int Id){

        Object object = new Object();
        Pull pull = new Pull();


        ArrayList<String> fields = new ArrayList<>();

        fields.add("id");


        String[] values = {String.valueOf(Id)};

        try {

            object = selectWhere(Pull.class,fields,values);

            pull = (Pull) object;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return pull;

    }


    public ArrayList<Pull> selectList(){
        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<Pull> lista = new ArrayList<>();

        try {
            objectList = selectList(Pull.class);

            lista = new ArrayList<Pull>();

            for(Object object : objectList){
                Pull pull = (Pull) object;
                lista.add(pull);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }


    public ArrayList<Pull> selectListbyRepo(int id){

        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<Pull> lista = new ArrayList<>();

        ArrayList<String> fields = new ArrayList<>();

        fields.add("idRepo");

        String[] values ={String.valueOf(id)};

        try {
            objectList = selectListWhere(Pull.class,fields,values);

            lista = new ArrayList<Pull>();

            for(Object object : objectList){
                Pull pull = (Pull) object;
                lista.add(pull);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }



}
