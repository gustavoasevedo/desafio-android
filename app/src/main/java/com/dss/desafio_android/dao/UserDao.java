package com.dss.desafio_android.dao;

import android.content.Context;

import com.dss.desafio_android.constant.ConstantDB;
import com.dss.desafio_android.model.User;
import com.dss.sdatabase.dao.BaseTable;
import com.dss.sdatabase.exceptions.InvalidTypeException;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class UserDao extends BaseTable {

    private static UserDao instance;

    public UserDao(Context context) {
        super(context, User.class, ConstantDB.getDbName(),ConstantDB.getVersion());
        createTable();
    }

    public static UserDao getInstance(Context context) {
        if (instance == null) {
            synchronized (UserDao.class) {
                if (instance == null) {
                    instance = new UserDao(context);
                }
            }
        }
        return instance;
    }

    public void insertObject(User user){

        ArrayList<Object> objectArrayList = new ArrayList<>();

        objectArrayList.add(user);

        insert(objectArrayList);
    }

    public void insertListObject(ArrayList<User> users){

        ArrayList<Object> objectArrayList = new ArrayList<>();

        for(Object object: users){
            objectArrayList.add(object);
        }

        insert(objectArrayList);
    }


    public User selectId(int Id){

        Object object = new Object();
        User user = new User();


        ArrayList<String> fields = new ArrayList<>();

        fields.add("id");


        String[] values = {String.valueOf(Id)};

        try {

            object = selectWhere(User.class,fields,values);

            user = (User) object;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return user;

    }


    public ArrayList<User> selectList(){
        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<User> lista = new ArrayList<>();

        try {
            objectList = selectList(User.class);

            lista = new ArrayList<User>();

            for(Object object : objectList){
                User user = (User) object;
                lista.add(user);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }


    public ArrayList<User> selectListbyName(String nome){

        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<User> lista = new ArrayList<>();

        ArrayList<String> fields = new ArrayList<>();

        fields.add("name");

        String[] values = {nome};

        try {
            objectList = selectListWhere(User.class,fields,values);

            lista = new ArrayList<User>();

            for(Object object : objectList){
                User user = (User) object;
                lista.add(user);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }



}
