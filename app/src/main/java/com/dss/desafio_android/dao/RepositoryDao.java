package com.dss.desafio_android.dao;

import android.content.Context;

import com.dss.desafio_android.constant.ConstantDB;
import com.dss.desafio_android.model.Repository;
import com.dss.sdatabase.dao.BaseTable;
import com.dss.sdatabase.exceptions.InvalidTypeException;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class RepositoryDao extends BaseTable {

    private static RepositoryDao instance;

    public RepositoryDao(Context context) {
        super(context, Repository.class, ConstantDB.getDbName(),ConstantDB.getVersion());
        createTable();
    }

    public static RepositoryDao getInstance(Context context) {
        if (instance == null) {
            synchronized (RepositoryDao.class) {
                if (instance == null) {
                    instance = new RepositoryDao(context);
                }
            }
        }
        return instance;
    }

    public void insertObject(Repository repository){

        ArrayList<Object> objectArrayList = new ArrayList<>();

        objectArrayList.add(repository);

        insert(objectArrayList);
    }

    public void insertListObject(ArrayList<Repository> repositories){

        ArrayList<Object> objectArrayList = new ArrayList<>();

        for(Object object: repositories){
            objectArrayList.add(object);
        }

        insert(objectArrayList);
    }


    public Repository selectId(int Id){

        Object object = new Object();
        Repository repository = new Repository();


        ArrayList<String> fields = new ArrayList<>();

        fields.add("id");


        String[] values = {String.valueOf(Id)};

        try {

            object = selectWhere(Repository.class,fields,values);

            repository = (Repository) object;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return repository;

    }


    public ArrayList<Repository> selectList(){
        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<Repository> lista = new ArrayList<>();

        try {
            objectList = selectList(Repository.class);

            lista = new ArrayList<Repository>();

            for(Object object : objectList){
                Repository repository = (Repository) object;
                lista.add(repository);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }


    public ArrayList<Repository> selectListbyLanguage(String language){

        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<Repository> lista = new ArrayList<>();

        ArrayList<String> fields = new ArrayList<>();

        fields.add("language");

        String[] values = {language};

        String query = "select * from " +  Repository.class.getSimpleName() + " where language = ? Order by stargazers Desc";

        try {
            objectList = selectRawQuery(Repository.class,query,values);

            lista = new ArrayList<Repository>();

            for(Object object : objectList){
                Repository repository = (Repository) object;
                lista.add(repository);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }


    public ArrayList<Repository> selectListbyPage(int page,String language){

        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<Repository> lista = new ArrayList<>();

        ArrayList<String> fields = new ArrayList<>();

        fields.add("page");
        fields.add("language");

        String[] values = {String.valueOf(page),language};

        String query = "select * from " +  Repository.class.getSimpleName() + " where page = ? and language = ? Order by stargazers Desc";

        try {
            objectList = selectRawQuery(Repository.class,query,values);

            lista = new ArrayList<Repository>();

            for(Object object : objectList){
                Repository repository = (Repository) object;
                lista.add(repository);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }

        return lista;
    }



}
