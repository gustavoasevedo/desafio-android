package com.dss.desafio_android.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.dss.desafio_android.R;
import com.dss.desafio_android.adapter.RepositoryListAdapter;
import com.dss.desafio_android.constant.ConstantFragment;
import com.dss.desafio_android.dao.RepositoryDao;
import com.dss.desafio_android.delegate.UpdateDelegate;
import com.dss.desafio_android.helper.RepositoryListFragmentHelper;
import com.dss.desafio_android.model.Repository;
import com.dss.desafio_android.tasks.ReposSyncTask;

import java.util.ArrayList;

/**
 * Created by gustavo.vieira on 22/05/2015.
 */
public class RepositoryListFragment extends Fragment implements UpdateDelegate {

    Context context;
    Activity activity;
    RepositoryListFragmentHelper helper;
    View view;
    RepositoryListAdapter adapter;
    ArrayList<Repository> lista;
    String language;
    int page;
    int currentFirstVisibleItem;
    int currentVisibleItemCount;
    int currentScrollState;
    Boolean atualizando = false;

    public RepositoryListFragment newInstance(String language) {
        RepositoryListFragment mFragment = new RepositoryListFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(ConstantFragment.getLANGUAGE(), language);
        mFragment.setArguments(mBundle);
        return mFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_repository, container, false);

        context = getActivity();
        activity = ((Activity)context);

        Bundle bundle = this.getArguments();
        language = bundle.getString(ConstantFragment.getLANGUAGE());

        helper = new RepositoryListFragmentHelper();

        page = 1;

        initLayout();

        configureAdapter();

        return view;
    }

    public void initLayout(){
        helper.ListFragment(view);

    }

    public void configureAdapter(){


        lista = new ArrayList<>();

        lista = RepositoryDao.getInstance(context).selectListbyLanguage(language);

        if(adapter == null) {
            adapter = new RepositoryListAdapter(context, R.layout.item_list_repository, lista);
            helper.setAdapter(adapter);
            helper.setListClickListener(listClickListener);

            helper.addTextChangedListener(textChangedListener);
            helper.setSwipeListener(onRefreshListener);
            helper.setListScrollListener(scrollListener);
        }else{
            adapter.notifyDataSetChanged();
        }
    }

    public AdapterView.OnItemClickListener listClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View v, int position,long arg3) {

            Repository repository = adapter.getFilteredObject().get(position);

            FragmentStarter.startPullListFragment(context,repository,language);

        }
    };

    public SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {

            page = 1;
            ReposSyncTask reposSyncTask = new ReposSyncTask(page, language,RepositoryListFragment.this);
            reposSyncTask.execute();

        }
    };

    public TextWatcher textChangedListener = new TextWatcher(){

        @Override
        public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                  int arg3) {
            // When user changed the Text
            adapter.getFilter().filter(cs);
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1,
                                      int arg2, int arg3) {
        }

        @Override
        public void afterTextChanged(Editable arg0) {
        }

    };


    @Override
    public void sucessoUpdate(boolean sucesso) {
        helper.finishSwipe();

        verifyPages();
    }

    @Override
    public void ErroUpdate(Exception e) {
        helper.finishSwipe();
        Toast.makeText(getActivity(),e.getMessage().toString(),Toast.LENGTH_LONG).show();
        page--;

    }

    public AbsListView.OnScrollListener scrollListener = new AbsListView.OnScrollListener() {

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            currentFirstVisibleItem = firstVisibleItem;
            currentVisibleItemCount = visibleItemCount;
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            currentScrollState = scrollState;
            isScrollCompleted();
        }

        private void isScrollCompleted() {
            int lastitem =  currentFirstVisibleItem + currentVisibleItemCount;
            if (currentVisibleItemCount > 0 && lista.size() == lastitem) {
                if (atualizando) {
                    return;
                } else {
                    page++;
                    ArrayList<Repository> repositories = RepositoryDao.getInstance(context).selectListbyPage(page,language);
                    if (repositories != null) {
                        if (repositories.size() > 0) {
                            lista.addAll(repositories);
                        } else {
                            ReposSyncTask reposSyncTask = new ReposSyncTask(page, language, RepositoryListFragment.this);
                            reposSyncTask.execute();
                            atualizando = true;
                        }

                    } else {
                        ReposSyncTask reposSyncTask = new ReposSyncTask(page, language, RepositoryListFragment.this);
                        reposSyncTask.execute();
                        atualizando = true;
                    }
                }
            }


        }
    };



    public void verifyPages(){
        if(page == 1) {
                lista = RepositoryDao.getInstance(context).selectListbyLanguage(language);
                configureAdapter();
                atualizando = false;

        }else{
            ArrayList<Repository> repositories = RepositoryDao.getInstance(context).selectListbyPage(page,language);
            if(repositories.size() > 0) {
                lista.addAll(repositories);
                adapter.notifyDataSetChanged();
                helper.setListClickListener(listClickListener);
                atualizando = false;
            }
        }
    }
}
