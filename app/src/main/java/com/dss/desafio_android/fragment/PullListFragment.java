package com.dss.desafio_android.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;


import com.dss.desafio_android.R;
import com.dss.desafio_android.adapter.PullListAdapter;
import com.dss.desafio_android.constant.ConstantFragment;
import com.dss.desafio_android.dao.OwnerDao;
import com.dss.desafio_android.dao.PullDao;
import com.dss.desafio_android.delegate.UpdateDelegate;
import com.dss.desafio_android.helper.PullListFragmentHelper;
import com.dss.desafio_android.model.Owner;
import com.dss.desafio_android.model.Pull;
import com.dss.desafio_android.model.Repository;
import com.dss.desafio_android.tasks.PullsSyncTask;

import java.util.ArrayList;

/**
 * Created by gustavo.vieira on 22/05/2015.
 */
public class PullListFragment extends Fragment implements UpdateDelegate {

    Context context;
    Activity activity;
    PullListFragmentHelper helper;
    View view;
    PullListAdapter adapter;
    ArrayList<Pull> lista;
    Repository repo;
    String language;
    Owner owner;

    public PullListFragment newInstance(Repository repository,String language) {
        PullListFragment mFragment = new PullListFragment();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(ConstantFragment.getREPOSITORY(), repository);
        mBundle.putString(ConstantFragment.getLANGUAGE(),language);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_pull, container, false);

        context = getActivity();
        activity = ((Activity)context);

        Bundle bundle = this.getArguments();
        repo = (Repository) bundle.getSerializable(ConstantFragment.getREPOSITORY());

        language = bundle.getString(ConstantFragment.getLANGUAGE());

        owner = OwnerDao.getInstance(context).selectId(repo.getIdOwner());

        helper = new PullListFragmentHelper();

        initLayout();

        lista = PullDao.getInstance(context).selectListbyRepo(repo.getId());

        if(lista.size() <= 0) {
            PullsSyncTask pullsSyncTask = new PullsSyncTask(owner.getLogin(), repo.getName(), repo.getId(), this);
            pullsSyncTask.execute();
        }else{
            configureAdapter();
        }

        return view;
    }

    public void initLayout(){
        helper.ListFragment(view);
        helper.setSwipeListener(onRefreshListener);

    }


    public SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            PullsSyncTask pullsSyncTask = new PullsSyncTask(owner.getLogin(), repo.getName(), repo.getId(), PullListFragment.this);
            pullsSyncTask.execute();
        }
    };


    public void configureAdapter(){

        adapter = new PullListAdapter(context, R.layout.item_list_pull, lista);
        helper.setAdapter(adapter);
        helper.setListClickListener(listClickListener);
        adapter.notifyDataSetChanged();

        helper.addTextChangedListener(textChangedListener);
    }

    public AdapterView.OnItemClickListener listClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View v, int position,long arg3) {
            Pull p = adapter.getFilteredObject().get(position);

            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(p.getUrl()));
            startActivity(i);
        }
    };

    public TextWatcher textChangedListener = new TextWatcher(){

        @Override
        public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                  int arg3) {
            // When user changed the Text
            adapter.getFilter().filter(cs);
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1,
                                      int arg2, int arg3) {
        }

        @Override
        public void afterTextChanged(Editable arg0) {
        }

    };


    @Override
    public void sucessoUpdate(boolean sucesso) {
        helper.finishSwipe();
        lista = PullDao.getInstance(context).selectListbyRepo(repo.getId());

        configureAdapter();
    }

    @Override
    public void ErroUpdate(Exception e) {

    }




}
