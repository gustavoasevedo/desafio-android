package com.dss.desafio_android.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.dss.desafio_android.R;
import com.dss.desafio_android.constant.ConstantFragment;
import com.dss.desafio_android.model.Pull;
import com.dss.desafio_android.model.Repository;


public abstract class FragmentStarter {

    public static Fragment startListFragment(Context context,String language) {

        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        RepositoryListFragment repositoryListFragment = new RepositoryListFragment().newInstance(language);
        fragmentTransaction.replace(R.id.fragment_container, repositoryListFragment, ConstantFragment.getListFragment());
        fragmentTransaction.commit();

        return repositoryListFragment;

    }

    public static Fragment startPullListFragment(Context context, Repository repository,String language) {

        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        PullListFragment pullListFragment = new PullListFragment().newInstance(repository,language);
        fragmentTransaction.replace(R.id.fragment_container, pullListFragment, ConstantFragment.getPullFragment());
        fragmentTransaction.commit();

        return pullListFragment;

    }

}
