package com.dss.desafio_android.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dss.desafio_android.R;
import com.dss.desafio_android.dao.OwnerDao;
import com.dss.desafio_android.dao.UserDao;
import com.dss.desafio_android.factories.ImageFactory;
import com.dss.desafio_android.model.Owner;
import com.dss.desafio_android.model.Pull;
import com.dss.desafio_android.model.User;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by gustavo.vieira on 20/01/2015.
 */
public class PullListAdapter extends ArrayAdapter<Pull> {

    private Context context;
    private int layoutResourceId;
    private List<Pull> lObject;
    private List<Pull> filteredObject;


    public PullListAdapter(Context context, int textViewResourceId,
                                 List<Pull> objects) {
        super(context, textViewResourceId, objects);
        this.setContext(context);
        this.setLayoutResourceId(textViewResourceId);
        this.setlObjects(objects);
        this.setFilteredObject(objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View row = convertView;
        ListClienteHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();

            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ListClienteHolder();

            holder.title = (TextView) row.findViewById(R.id.txtListTitle);

            holder.descricao = (TextView) row.findViewById(R.id.txtDescricao);

            holder.nameUser = (TextView) row.findViewById(R.id.txtListNameUser);

            holder.imgUser = (ImageView) row.findViewById(R.id.imgUser);
            holder.txtData = (TextView) row.findViewById(R.id.txtData);

            row.setTag(holder);
        } else {
            holder = (ListClienteHolder) row.getTag();
        }

        Pull pull = filteredObject.get(position);

        holder.title.setText(String.valueOf(pull.getTitle()));

        holder.descricao.setText(pull.getBody());

        final User user = UserDao.getInstance(context).selectId(pull.getIdUser());

        String nomeUser = user.getLogin();

        holder.nameUser.setText(nomeUser);

        if(user.getAvatar() != null) {
            if(user.getAvatar() != "") {
                Glide.with(context)
                        .load(user.getAvatar())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .crossFade()
                        .into(holder.imgUser);
            }
        }

        holder.imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageFactory.showDialogImage(context,user.getLogin(),user.getAvatar());
            }
        });

        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(pull.getDate());
            String formattedTime = output.format(d);

            holder.txtData.setText(formattedTime);
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }







        return row;

    }

    public List<Pull> getFilteredObject() {
        return filteredObject;
    }

    public void setFilteredObject(List<Pull> filteredObject) {
        this.filteredObject = filteredObject;
    }

    static class ListClienteHolder {
        TextView title,nameUser,descricao,txtData;
        ImageView imgUser;
    }

    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getLayoutResourceId() {
        return layoutResourceId;
    }

    public void setLayoutResourceId(int layoutResourceId) {
        this.layoutResourceId = layoutResourceId;
    }

    public List<Pull> getlObjects() {
        return filteredObject;
    }

    public void setlObjects(List<Pull> lNoticias) {
        this.lObject = lNoticias;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return filteredObject.size();
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {

                // arrayListNames = (List<String>) results.values;
                filteredObject = (List<Pull>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                List<Pull> FilteredArrayNames = new ArrayList<Pull>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < lObject.size(); i++) {
                    Pull object = lObject.get(i);
                    String dataNames = object.getTitle();

                    if (dataNames.toLowerCase().contains(constraint.toString())) {
                        FilteredArrayNames.add(object);
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;

                return results;
            }
        };

        return filter;
    }

}
