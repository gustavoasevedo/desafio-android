package com.dss.desafio_android.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dss.desafio_android.R;
import com.dss.desafio_android.dao.OwnerDao;
import com.dss.desafio_android.factories.ImageFactory;
import com.dss.desafio_android.model.Owner;
import com.dss.desafio_android.model.Repository;

import java.util.ArrayList;
import java.util.List;

/**
* Created by gustavo.vieira on 20/01/2015.
*/
public class RepositoryListAdapter extends ArrayAdapter<Repository> {

    private Context context;
    private int layoutResourceId;
    private List<Repository> lObject;
    private List<Repository> filteredObject;


    public RepositoryListAdapter(Context context, int textViewResourceId,
                                 List<Repository> objects) {
        super(context, textViewResourceId, objects);
        this.setContext(context);
        this.setLayoutResourceId(textViewResourceId);
        this.setlObjects(objects);
        this.setFilteredObject(objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View row = convertView;
        ListClienteHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();

            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ListClienteHolder();

            holder.title = (TextView) row.findViewById(R.id.txtListTitle);

            holder.descricao = (TextView) row.findViewById(R.id.txtDescricao);

            holder.nameUser = (TextView) row.findViewById(R.id.txtListNameUser);

            holder.fork = (TextView) row.findViewById(R.id.txtForks);

            holder.stargazers = (TextView) row.findViewById(R.id.txtStargazers);

            holder.imgUser = (ImageView) row.findViewById(R.id.imgUser);

            row.setTag(holder);
        } else {
            holder = (ListClienteHolder) row.getTag();
        }

        Repository repository = filteredObject.get(position);

        holder.title.setText(String.valueOf(repository.getName()));

        holder.stargazers.setText(String.valueOf(repository.getStargazers()));

        holder.fork.setText(String.valueOf(repository.getForks()));

        holder.descricao.setText(repository.getDescription());

        final Owner owner = OwnerDao.getInstance(context).selectId(repository.getIdOwner());

        String nomeUser = owner.getLogin();

        holder.nameUser.setText(nomeUser);

        if(owner.getAvatar() != null) {
            if(owner.getAvatar() != "") {
                Glide.with(context)
                        .load(owner.getAvatar())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .crossFade()
                        .into(holder.imgUser);
            }
        }

        holder.imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageFactory.showDialogImage(context,owner.getLogin(),owner.getAvatar());
            }
        });



        return row;

    }

    public List<Repository> getFilteredObject() {
        return filteredObject;
    }

    public void setFilteredObject(List<Repository> filteredObject) {
        this.filteredObject = filteredObject;
    }

    static class ListClienteHolder {
        TextView title,nameUser,fork,stargazers,descricao;
        ImageView imgUser;
    }

    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getLayoutResourceId() {
        return layoutResourceId;
    }

    public void setLayoutResourceId(int layoutResourceId) {
        this.layoutResourceId = layoutResourceId;
    }

    public List<Repository> getlObjects() {
        return filteredObject;
    }

    public void setlObjects(List<Repository> lNoticias) {
        this.lObject = lNoticias;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return filteredObject.size();
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {

                // arrayListNames = (List<String>) results.values;
                filteredObject = (List<Repository>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                List<Repository> FilteredArrayNames = new ArrayList<Repository>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < lObject.size(); i++) {
                    Repository object = lObject.get(i);
                    String dataNames = object.getFullName();

                    if (dataNames.toLowerCase().contains(constraint.toString())) {
                        FilteredArrayNames.add(object);
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;

                return results;
            }
        };

        return filter;
    }

}
