package com.dss.desafio_android.delegate;

import android.content.Context;

public interface UpdateDelegate {
    void sucessoUpdate(boolean sucesso);
    void ErroUpdate(Exception e);
    Context getContext();
}
