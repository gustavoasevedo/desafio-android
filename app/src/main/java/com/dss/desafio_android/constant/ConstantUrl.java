package com.dss.desafio_android.constant;

/**
 * Created by Gustavo on 30/12/2015.
 */
public abstract class ConstantUrl {

    private static String URL_PULLS = "https://api.github.com/repos/";

    private static String URL_PULLS_GET = "/pulls";

    private static String URL_LANGUAGES = "https://api.github.com/search/repositories?q=language:";

    private static String URL_LANGUAGES_JAVA = "Java";

    private static String URL_LANGUAGES_Ruby= "Ruby";

    private static String URL_LANGUAGES_Python = "Python";

    private static String  URL_LANGUAGES_SORT = "&sort=stars&page=";



    public static String getUrlPulls() {
        return URL_PULLS;
    }

    public static String getUrlLanguages() {
        return URL_LANGUAGES;
    }

    public static String getUrlPullsGet() {
        return URL_PULLS_GET;
    }

    public static String getUrlLanguagesJava() {
        return URL_LANGUAGES_JAVA;
    }

    public static String getURL_LANGUAGES_Ruby() {
        return URL_LANGUAGES_Ruby;
    }

    public static String getURL_LANGUAGES_Python() {
        return URL_LANGUAGES_Python;
    }

    public static String getUrlLanguagesSort() {
        return URL_LANGUAGES_SORT;
    }

}

