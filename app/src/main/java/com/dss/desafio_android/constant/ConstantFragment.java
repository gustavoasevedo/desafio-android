package com.dss.desafio_android.constant;

/**
 * Created by gustavo.vieira on 11/05/2015.
 */
public abstract class ConstantFragment {

    private static final String LANGUAGE = "LANGUAGE";

    private static final String REPOSITORY = "REPOSITORY";

    private static final String LIST_FRAGMENT = "listFragment";

    private static final String PULL_FRAGMENT = "pullFragment";



    public static String getLANGUAGE() {
        return LANGUAGE;
    }

    public static String getREPOSITORY() {
        return REPOSITORY;
    }

    public static String getListFragment() {
        return LIST_FRAGMENT;
    }

    public static String getPullFragment() {
        return PULL_FRAGMENT;
    }
}
