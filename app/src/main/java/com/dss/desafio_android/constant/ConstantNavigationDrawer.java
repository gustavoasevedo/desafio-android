package com.dss.desafio_android.constant;

import com.dss.desafio_android.R;

/**
 * Created by gustavo.vieira on 22/05/2015.
 */
public class ConstantNavigationDrawer {

    private static final String[] TITLES =
            {"Java",
                    "Ruby",
                    "Python"};

    private static final int[] ICONS =
            {R.drawable.github_small,
                    R.drawable.github_small,
                    R.drawable.github_small};

    private static final String NAME = "Gustavo Asevedo Vieira";

    private static final String EMAIL = "gustavo.asevedo@gmail.com";

    private static final int PROFILE = R.drawable.github;


    public static String[] getTITLES() {
        return TITLES;
    }

    public static int[] getICONS() {
        return ICONS;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getEMAIL() {
        return EMAIL;
    }

    public static int getPROFILE() {
        return PROFILE;
    }
}
